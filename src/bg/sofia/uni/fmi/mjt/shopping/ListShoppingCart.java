package bg.sofia.uni.fmi.mjt.shopping;

import java.util.*;

import bg.sofia.uni.fmi.mjt.shopping.item.Item;

public class ListShoppingCart implements ShoppingCart {

    private List<Item> items = new ArrayList<>();

    @Override
    public Collection<Item> getUniqueItems() {
        Set<Item> uniqueItems = new HashSet<>();

        for (Item item : items) {
            uniqueItems.add(item);
        }

        return uniqueItems;
    }

    @Override
    public void addItem(Item item) {
        if (item != null) {
            items.add(item);
        }
    }

    @Override
    public void removeItem(Item item) throws ItemNotFoundException {
        if (!items.contains(item)) {
            //TODO override to string and add exception message
            throw new ItemNotFoundException();
        }
        
        for (Item currentItem : items) {
            if (currentItem.equals(item)) {
                items.remove(currentItem);
                break;
            }
        }
    }

    @Override
    public double getTotal() {
        double total = 0;

        for(Item item : items){
            total =+ item.getPrice();
        }

        return total;
    }

    private Map<Item,Integer> createIdentityMap() {
        Map<Item, Integer> identityMap = new HashMap<>();

        for (Item item : items) {
            //TODO map merge(), no if else
          if (!identityMap.containsKey(item)) {
              identityMap.put(item, 1);
          } else {
              identityMap.put(item, identityMap.get(item) + 1);
          }
        }

        return identityMap;
    }

    @Override
    public Set<Item> getSortedItems() {
        Map<Item, Integer> identityMap = createIdentityMap();

        //TODO Treemap, sort them by values
        List<Map.Entry<Item, Integer>> list = new ArrayList(identityMap.entrySet());
        list.sort(Map.Entry.comparingByValue());
        Collections.reverse(list);

        Set<Item> sortedItems = new LinkedHashSet();

        for (Map.Entry<Item, Integer> entry : list) {
            sortedItems.add(entry.getKey());
        }

        return sortedItems;
    }
}
