package bg.sofia.uni.fmi.mjt.shopping.item;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ItemTest {

    @Test
    public void testEqualsWithIdenticalChocolateAndApple() {
        Item Apple1 = new Apple("orange", "very tasty", 3.5);
        Item Chocolate1 = new Chocolate("orange", "very tasty", 3.5);

        assertNotEquals(Apple1, Chocolate1);
    }

    @Test
    public void testEqualsWithIdenticalItems() {
        Item Apple1 = new Apple("orange", "very tasty", 3.5);
        Item Apple2 = new Apple("orange", "very tasty", 3.5);

        assertEquals(Apple1, Apple2);
    }

    @Test
    public void testEqualsWithDifferentItems() {
        Item Apple1 = new Apple("orange", "very tasty", 3.5);
        Item Apple2 = new Apple("purple", "soo tasty", 1.3);

        assertNotEquals(Apple1, Apple2);
    }
}
