package bg.sofia.uni.fmi.mjt.shopping;

import bg.sofia.uni.fmi.mjt.shopping.item.Apple;
import bg.sofia.uni.fmi.mjt.shopping.item.Chocolate;
import bg.sofia.uni.fmi.mjt.shopping.item.Item;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MapShoppingCartTest {

    protected MapShoppingCartMock mapShoppingCartMock;

    protected static class MapShoppingCartMock extends MapShoppingCart {

        protected Map<Item, Integer> items;

        //TODO nono reflection

        MapShoppingCartMock() throws NoSuchFieldException, IllegalAccessException {
            super();
            Field f = this.getClass().getSuperclass().getDeclaredField("items");
            f.setAccessible(true);
            this.items = (Map<Item, Integer>) f.get(this);
        }
    }

    @Before
    public void init() throws NoSuchFieldException, IllegalAccessException {
        mapShoppingCartMock = new MapShoppingCartMock();
    }

    @Test
    public void testGetUniqueItemsWithDuplicates() {
        //TODO add apple 1 twice instead of apple1 and apple2
        Apple apple1 = new Apple("apple1", "apple1 desc", 1);
        Apple apple2 = new Apple("apple1", "apple1 desc", 1);
        Chocolate chocolate1 = new Chocolate("chocolate1", "chocolate1 desc", 17);
        Chocolate chocolate2 = new Chocolate("chocolate1", "chocolate1 desc", 17);
        mapShoppingCartMock.addItem(apple1);
        mapShoppingCartMock.addItem(apple2);
        mapShoppingCartMock.addItem(chocolate1);
        mapShoppingCartMock.addItem(chocolate2);

        assertTrue(mapShoppingCartMock.getUniqueItems().size() == 2);
    }

    @Test
    public void testGetUniqueItemsWithoutDuplicates() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1);
        Apple apple2 = new Apple("apple2", "apple2 desc", 2);
        Chocolate chocolate1 = new Chocolate("chocolate1", "chocolate1 desc", 17);
        Chocolate chocolate2 = new Chocolate("chocolate2", "chocolate2 desc", 77);
        mapShoppingCartMock.addItem(apple1);
        mapShoppingCartMock.addItem(apple2);
        mapShoppingCartMock.addItem(chocolate1);
        mapShoppingCartMock.addItem(chocolate2);

        assertTrue(mapShoppingCartMock.getUniqueItems().size() == 4);
    }

    @Test
    public void testGetUniqueItemsWithNull() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1);
        Apple apple2 = null;
        mapShoppingCartMock.addItem(apple1);
        mapShoppingCartMock.addItem(apple2);

        assertTrue(mapShoppingCartMock.getUniqueItems().size() == 1);
    }

    @Test
    public void testAddItemWithUniqueItems() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple2", "apple2 desc", 2.6);
        mapShoppingCartMock.addItem(apple1);
        mapShoppingCartMock.addItem(apple2);

        assertTrue(mapShoppingCartMock.getUniqueItems().size() == 2);
    }

    @Test
    public void testAddItemWithDuplicateItems() throws NoSuchFieldException, IllegalAccessException {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple1", "apple1 desc", 1.0);
        mapShoppingCartMock.addItem(apple1);
        mapShoppingCartMock.addItem(apple2);

        assertTrue(mapShoppingCartMock.items.get(apple1) == 2);
    }

    @Test
    public void testAddItemWithNull() throws NoSuchFieldException, IllegalAccessException {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1);
        mapShoppingCartMock.addItem(apple1);
        mapShoppingCartMock.addItem(null);

        assertTrue(mapShoppingCartMock.items.size() == 1);
    }

    @Test(expected = ItemNotFoundException.class)
    public void testRemoveItemWithNonexistentItem() throws ItemNotFoundException {
        Apple apple2 = new Apple("apple2", "apple2 desc", 2.6);
        mapShoppingCartMock.removeItem(apple2);
    }

    @Test
    public void testRemoveUniqueItem() throws ItemNotFoundException, NoSuchFieldException, IllegalAccessException {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple2", "apple2 desc", 2.6);
        mapShoppingCartMock.addItem(apple1);
        mapShoppingCartMock.addItem(apple2);
        mapShoppingCartMock.removeItem(apple1);

        assertTrue(mapShoppingCartMock.items.size() == 1);
    }

    @Test
    public void testRemoveItemWithDuplicate() throws ItemNotFoundException, NoSuchFieldException, IllegalAccessException {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple1", "apple1 desc", 1.0);
        mapShoppingCartMock.addItem(apple1);
        mapShoppingCartMock.addItem(apple2);
        mapShoppingCartMock.removeItem(apple2);

        assertTrue(mapShoppingCartMock.items.size() == 1);
    }

    @Test(expected = ItemNotFoundException.class)
    public void testRemoveNullItem() throws ItemNotFoundException {
        mapShoppingCartMock.removeItem(null);
    }

    @Test
    public void testGetSortedItemsWithDifferentDuplicates() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple3 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple4 = new Apple("apple4", "apple4 desc", 3.3);
        Apple apple5 = new Apple("apple4", "apple4 desc", 3.3);
        Apple apple6 = new Apple("apple6", "apple6 desc", 4.4);
        mapShoppingCartMock.addItem(apple1);
        mapShoppingCartMock.addItem(apple2);
        mapShoppingCartMock.addItem(apple6);
        mapShoppingCartMock.addItem(apple3);
        mapShoppingCartMock.addItem(apple5);
        mapShoppingCartMock.addItem(apple4);

        Collection<Item> actual = mapShoppingCartMock.getSortedItems();
        //TODO iter iterator, same test!!
        Iterator<Item> iter = actual.iterator();
        assertEquals(3, actual.size());
        assertEquals(iter.next(), apple1);
        assertEquals(iter.next(), apple4);
        assertEquals(iter.next(), apple6);
    }

    @Test
    public void testGetSortedItemsWithDuplicates() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple3 = new Apple("apple1", "apple1 desc", 1.0);
        mapShoppingCartMock.addItem(apple1);
        mapShoppingCartMock.addItem(apple2);
        mapShoppingCartMock.addItem(apple3);

        Collection<Item> actual = mapShoppingCartMock.getSortedItems();
        Iterator<Item> iter = actual.iterator();
        assertEquals(1, actual.size());
        assertEquals(iter.next(), apple1);
    }

    @Test
    public void testGetSortedItemsWithEmptyCart() {
        Collection<Item> actual = mapShoppingCartMock.getSortedItems();
        assertEquals(0, actual.size());
    }

    @Test
    public void testGetSortedItemsWithNulls() {
        mapShoppingCartMock.addItem(null);
        Collection<Item> actual = mapShoppingCartMock.getSortedItems();
        assertEquals(0, actual.size());
    }
}
