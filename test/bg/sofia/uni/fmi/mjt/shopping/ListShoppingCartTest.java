package bg.sofia.uni.fmi.mjt.shopping;

import bg.sofia.uni.fmi.mjt.shopping.item.Apple;
import bg.sofia.uni.fmi.mjt.shopping.item.Chocolate;
import bg.sofia.uni.fmi.mjt.shopping.item.Item;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ListShoppingCartTest {

    //TODO in before class apples and chocolates
    //TODO expected actual local vars in the tests, messages, assert equals or assert that, readability

    private ListShoppingCartMock listShoppingCartMock;

    static class ListShoppingCartMock extends ListShoppingCart {

        ListShoppingCartMock() {
            super();
        }
        //TODO no reflection
        public int getListShioppingCartSize() throws NoSuchFieldException, IllegalAccessException {
            Field f = this.getClass().getSuperclass().getDeclaredField("list");
            f.setAccessible(true);

            return ((ArrayList<Item>) f.get(this)).size();
        }
    }

    @Before
    public void init() {
        this.listShoppingCartMock = new ListShoppingCartMock();
    }

    @Test
    public void testGetUniqueItemsWithDuplicates() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1);
        Apple apple2 = new Apple("apple1", "apple1 desc", 1);
        Chocolate chocolate1 = new Chocolate("chocolate1", "chocolate1 desc", 17);
        Chocolate chocolate2 = new Chocolate("chocolate1", "chocolate1 desc", 17);
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(apple2);
        listShoppingCartMock.addItem(chocolate1);
        listShoppingCartMock.addItem(chocolate2);

        assertTrue(listShoppingCartMock.getUniqueItems().size() == 2);
    }

    @Test
    public void testGetUniqueItemsWithoutDuplicates() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1);
        Apple apple2 = new Apple("apple2", "apple2 desc", 2);
        Chocolate chocolate1 = new Chocolate("chocolate1", "chocolate1 desc", 17);
        Chocolate chocolate2 = new Chocolate("chocolate2", "chocolate2 desc", 77);
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(apple2);
        listShoppingCartMock.addItem(chocolate1);
        listShoppingCartMock.addItem(chocolate2);

        assertTrue(listShoppingCartMock.getUniqueItems().size() == 4);
    }

    @Test
    public void testGetUniqueItemsWithNull() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1);
        Apple apple2 = null;
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(apple2);

        assertTrue(listShoppingCartMock.getUniqueItems().size() == 1);
    }

    @Test
    public void testAddItemWithUniqueItems() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple2", "apple2 desc", 2.6);
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(apple2);

        assertTrue(listShoppingCartMock.getUniqueItems().size() == 2);
    }

    @Test
    public void testAddItemWithDuplicateItems() throws NoSuchFieldException, IllegalAccessException {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple1", "apple1 desc", 1.0);
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(apple2);

        assertTrue(listShoppingCartMock.getListShioppingCartSize() == 2);
    }

    @Test
    public void testAddItemWithNull() throws NoSuchFieldException, IllegalAccessException {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1);
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(null);

        assertTrue(listShoppingCartMock.getListShioppingCartSize() == 1);
    }

    @Test(expected = ItemNotFoundException.class)
    public void testRemoveItemWithNonexistentItem() throws ItemNotFoundException {
        Apple apple2 = new Apple("apple2", "apple2 desc", 2.6);
        listShoppingCartMock.removeItem(apple2);
    }

    @Test
    public void testRemoveUniqueItem() throws ItemNotFoundException, NoSuchFieldException, IllegalAccessException {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple2", "apple2 desc", 2.6);
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(apple2);
        listShoppingCartMock.removeItem(apple1);

        assertTrue(listShoppingCartMock.getListShioppingCartSize() == 1);
    }

    @Test
    public void testRemoveItemWithDuplicate() throws ItemNotFoundException, NoSuchFieldException, IllegalAccessException {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple1", "apple1 desc", 1.0);
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(apple2);
        listShoppingCartMock.removeItem(apple2);

        assertTrue(listShoppingCartMock.getListShioppingCartSize() == 1);
    }

    @Test(expected = ItemNotFoundException.class)
    public void testRemoveNullItem() throws ItemNotFoundException {
        listShoppingCartMock.removeItem(null);
    }

    @Test
    public void testGetSortedItemsWithDifferentDuplicates() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple3 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple4 = new Apple("apple4", "apple4 desc", 3.3);
        Apple apple5 = new Apple("apple4", "apple4 desc", 3.3);
        Apple apple6 = new Apple("apple6", "apple6 desc", 4.4);
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(apple2);
        listShoppingCartMock.addItem(apple6);
        listShoppingCartMock.addItem(apple3);
        listShoppingCartMock.addItem(apple5);
        listShoppingCartMock.addItem(apple4);

        Set<Item> actual = listShoppingCartMock.getSortedItems();
        //TODO no-no linkedhashset,
        assertTrue(actual instanceof LinkedHashSet<?>);
        //TODO iterator not iter
        Iterator<Item> iter = actual.iterator();
        assertEquals(3, actual.size());
        assertEquals(iter.next(), apple1); // Note apple1/2/3 should have the same hashcode -> equal
        assertEquals(iter.next(), apple4);
        assertEquals(iter.next(), apple6);
    }

    @Test
    public void testGetSortedItemsWithDuplicates() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple2 = new Apple("apple1", "apple1 desc", 1.0);
        Apple apple3 = new Apple("apple1", "apple1 desc", 1.0);
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(apple2);
        listShoppingCartMock.addItem(apple3);

        Set<Item> actual = listShoppingCartMock.getSortedItems();
        Iterator<Item> iter = actual.iterator();
        assertEquals(1, actual.size());
        assertEquals(iter.next(), apple1);
    }

    @Test
    public void testGetSortedItemsWithUniqueOnly() {
        Apple apple1 = new Apple("apple1", "apple1 desc", 1.0);
        Chocolate chocolate = new Chocolate("c1", "c desc", 2.0);
        listShoppingCartMock.addItem(apple1);
        listShoppingCartMock.addItem(chocolate);

        LinkedHashSet<Item> actual = (LinkedHashSet<Item>) listShoppingCartMock.getSortedItems();
        System.out.println(actual.toString());
        System.out.println(actual.size());

        //In case they are unique, the order is the same as were added.
        assertEquals(2, actual.size());
    }

    @Test
    public void testGetSortedItemsWithEmptyCart() {
        Set<Item> actual = listShoppingCartMock.getSortedItems();
        assertEquals(0, actual.size());
    }

    @Test
    public void testGetSortedItemsWithNulls() {
        listShoppingCartMock.addItem(null);
        Set<Item> actual = listShoppingCartMock.getSortedItems();
        assertEquals(0, actual.size());
    }
}
